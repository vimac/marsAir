# Prerequisites
Install node 

Install npm, update npm 

# Install TestCafe from npm
```
npm install -g testcafe
```
Clone the repo: 
```
git clone https://gitlab.com/vimac/marsAir.git
```
Check Testcafe version: 
```
testcafe -v
```

Refer: https://testcafe.io/documentation/402834/guides/basic-guides/install-testcafe

# Run Tests
Run tests with chrome: `testcafe chrome testScript/Spec/home.spec.js`

